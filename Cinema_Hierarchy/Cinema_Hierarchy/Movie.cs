﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema_Hierarchy
{
    class Movie
    {
        public Movie (string title, string director, double duration)
        {
            this.Title = title;
            this.Director = director;
            this.Duration = duration;
        }
        public string Title { get; set; }
        public string Director { get; set; }
        public double Duration;
        //enum Ganre { Drama,Thriller,Horror,Comedy,Melo,Scifi }
    }
}
