﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema_Hierarchy
{
    class Seat
    {
        public Seat(int row, int colum)
        {
            this.Row = row;
            this.Colum = colum;
            this.isFree = false;
        } 
        private int Row { get; set; }
        private int Colum { get; set; }
        private bool isFree;
    }
}
