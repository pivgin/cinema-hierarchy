﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema_Hierarchy
{
    class Program
    {
        static void Main(string[] args)
        {
            Movie movie1 = new Movie("Se7en","Fincher",125);
            Movie movie2 = new Movie("Saw","Lee",111);
            Movie movie3 = new Movie("Batman","Nolan",186);

            Hall firstHall = new Hall(200,10,20,3);
            Hall secondHall = new Hall(100,10,10,2);

            Ticket ticket1 = new Ticket(movie1,DateTime.Now);
            Ticket ticket2 = new Ticket(movie1, DateTime.Now);
            Ticket ticket3 = new Ticket(movie2, DateTime.Now);
            Ticket ticket4 = new Ticket(movie3, DateTime.Now);

            Cinema Mir =new Cinema("Mir","Minsk, Kozlova str., 17");
            Mir.AddMovie(movie1);
            Mir.AddMovie(movie2);
            Mir.AddMovie(movie3);

        }
    }
}
