﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema_Hierarchy
{
    class Cinema
    {
        public string Name { get; set; }
        public string Address { get; set; }
        private List<Movie> Movies = new List<Movie>();
        private List<Ticket> Tickets = new List<Ticket>();
        private List<Hall> Halls =new List<Hall>(); 

        public Cinema(string name, string address)
        {
            this.Name = name;
            this.Address = address;
            //this.Movies = Movies;
        }

        public void AddMovie(Movie movie)
        {
            Movies.Add(movie);
        }  

    }
}
