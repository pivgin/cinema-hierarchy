﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema_Hierarchy
{
    class Hall
    {
        private int seats { get; set; }
        private int NumberOfRows { get; set; }
        private int NumberOfCollums { get; set; }
        private int Floor { get; set; }

        public Hall(int seats, int rows, int cols, int floor)
        {
            this.seats = seats;
            this.NumberOfCollums = cols;
            this.NumberOfRows = rows;
            this.Floor = floor;
        }
    }
}
